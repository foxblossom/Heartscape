extends Node2D

signal combo_completed(stick, combo)
signal combo_updated(stick, combo)
signal dual_combo_completed(leftCombo, rightCombo)

var last_left_combo  = ""
var last_right_combo = ""

const DUAL_GESTURE_FRAMES      = 3
var dual_gesture_current_frame = 0

func _process(delta):
	if dual_gesture_current_frame > 0:
		dual_gesture_current_frame -= 1


func checkDualCombo():
	if dual_gesture_current_frame > 0:
		emit_signal("dual_combo_completed", last_left_combo, last_right_combo)
		dual_gesture_current_frame = 0
		print("DUAL COMBO - " + last_left_combo + ", " + last_right_combo)
	

func _on_LeftStick_combo_complete(combo):
	emit_signal("combo_completed", Enums.EStickControl.Left, combo)
	last_left_combo = combo
	checkDualCombo()
	dual_gesture_current_frame = DUAL_GESTURE_FRAMES

func _on_RightStick_combo_complete(combo):
	emit_signal("combo_completed", Enums.EStickControl.Right, combo)
	last_right_combo = combo
	checkDualCombo()
	dual_gesture_current_frame = DUAL_GESTURE_FRAMES

func _on_LeftStick_combo_updated(combo):
	emit_signal("combo_updated", Enums.EStickControl.Left, combo)

func _on_RightStick_combo_updated(combo):
	emit_signal("combo_updated", Enums.EStickControl.Right, combo)
