

static func getStickAngleVector(stick):
	var stick_x = JOY_ANALOG_LX
	var stick_y = JOY_ANALOG_LY
	
	if (stick == Enums.EStickControl.Right):
		stick_x = JOY_ANALOG_RX
		stick_y = JOY_ANALOG_RY
		
	var stick_rot_x = Input.get_joy_axis(0, stick_x)
	var stick_rot_y = Input.get_joy_axis(0, stick_y)
	
	return Vector2(stick_rot_x, stick_rot_y)
