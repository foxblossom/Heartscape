extends Node2D

func _on_GestureManager_dual_combo_completed(leftCombo, rightCombo):	
	if leftCombo == "23456" and rightCombo == "65432":
		WriteText("Sword Combo")
	
	if leftCombo.length() >= 8 and rightCombo.length() >= 8:

		var first_four_l = leftCombo.substr(0, 4)
		var first_four_r = rightCombo.substr(0, 4)
		var last_four_l  = leftCombo.substr(4, 4)
		var last_four_r  = rightCombo.substr(4, 4)
		
		if first_four_l == last_four_r and first_four_r == last_four_l:
			WriteText("LanceCombo")

func WriteText(text):
	$CanvasLayer/Control/Label.text = text
	$Timer.start()

func _on_Timer_timeout():
	$CanvasLayer/Control/Label.text = ""
