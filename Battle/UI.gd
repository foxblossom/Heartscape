extends Control

func _ready():
	$Control/MPBarContainer/RightMP.tint_progress = PlayerPrefs.RIGHT_COLOUR
	$Control/MPBarContainer/LeftMP.tint_progress  = PlayerPrefs.LEFT_COLOUR

func _on_PlayerStats_hp_changed(hp):
	$Control/HPBar.value = hp

func _on_PlayerStats_left_mp_changed(mp):
	$Control/MPBarContainer/LeftMP.value = mp

func _on_PlayerStats_right_mp_changed(mp):
	$Control/MPBarContainer/RightMP.value = mp
