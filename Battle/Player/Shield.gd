extends Node2D

const InputUtility = preload("res://Utility/InputUtility.gd")

signal absorbed_bullet

export(Enums.EStickControl) var stick  = Enums.EStickControl.Left
export(Enums.EAttackColour) var colour = Enums.EAttackColour.None

const deadzone = 0.7

func _ready():
	if stick == Enums.EStickControl.Left:
		$ShieldSprite.modulate = PlayerPrefs.LEFT_COLOUR
	else:
		$ShieldSprite.modulate = PlayerPrefs.RIGHT_COLOUR

func _process(delta):
	var stick_rot = InputUtility.getStickAngleVector(stick)
	
	if (abs(stick_rot.x) + abs(stick_rot.y) < 0.7):
		return
	
	var angle = stick_rot.angle()
	
	rotation = angle

func absorb():
	emit_signal("absorbed_bullet")
