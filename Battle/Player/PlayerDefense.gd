extends Node2D

signal damage_taken(damage)
signal left_absorbed_bullet
signal right_absorbed_bullet

func _on_Heart_damage_taken(damage):
	emit_signal("damage_taken", damage)

func _on_RightShield_absorbed_bullet():
	emit_signal("right_absorbed_bullet")

func _on_LeftShield_absorbed_bullet():
	emit_signal("left_absorbed_bullet")
