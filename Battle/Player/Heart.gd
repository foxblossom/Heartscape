extends KinematicBody2D

signal damage_taken(damage)

var recovery_frames = false

# Damage player & activate recoery frames
func damage(damage):
	if !recovery_frames:
		print("damage")
		recovery_frames = true
		$RecoveryTime.start()
		$Sprite.modulate = Color.red
		emit_signal("damage_taken", damage)

# Recovery frame timer completion event
func _onRecoveryFramesComplete():
	recovery_frames = false
	$Sprite.modulate = Color.white
