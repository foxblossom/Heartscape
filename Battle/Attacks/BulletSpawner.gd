extends Node2D

const delay = 0.5
var   timer = 0

const bullet = preload("res://Battle/Attacks/Bullet.tscn")

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	timer += delta
	
	if (timer >= delay):
		timer -= delay
		fire()
		
func fire(): 
	add_child(bullet.instance())
